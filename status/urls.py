from django.urls import path
from . import views
urlpatterns = [
    path("", views.index, name="index"),
    path("<int:id>", views.index, name="index"),
    path("member", views.set_member, name="set_member"),
    path("add", views.add_status, name="add_status"),
    path("edit/<int:id>", views.edit_status, name="edit_status"),
    path("delete/<int:id>", views.delete_status, name="delete_status"),
    path("member/delete/<int:id>", views.delete_member, name="delete_member"),
    path("member/offline", views.add_offline_member, name="add_offline_member"),
    path("member/offline/delete/<int:id>", views.delete_offline_member, name="delete_offline_member"),
    path("email/browser", views.create_email_within_browser, name="create_email_within_browser"),
    path("email/outlook", views.create_email_in_outlook, name="create_email_in_outlook"),
]

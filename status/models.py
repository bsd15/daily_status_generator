from django.db import models

# Create your models here.


class Role(models.IntegerChoices):
    developer = 1
    tester = 2


class AbstractMember(models.Model):
    name = models.CharField(max_length=100, unique=True)
    role = models.IntegerField(choices=Role.choices)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Member(AbstractMember):
    class Meta:
        verbose_name = 'Member'
        verbose_name_plural = 'Members'

    def __str__(self):
        return self.name


class OfflineMember(AbstractMember):

    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "offlinemember"
        verbose_name_plural = "offlinemembers"

    def __str__(self):
        return self.name


class Status(models.Model):

    incident = models.CharField(max_length=100, blank=True)
    description = models.TextField()
    severity = models.CharField(max_length=50, blank=True)
    progress = models.CharField(max_length=200, blank=True)
    comment = models.TextField(blank=True)
    member = models.ForeignKey("Member", on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Status'
        verbose_name_plural = 'Status'

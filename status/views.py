from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.template import loader
from django.utils import timezone
from django.views.decorators.http import require_GET, require_POST
import json
import win32com.client as win32
import pythoncom
from yaml import load, FullLoader
from .models import Member, OfflineMember, Status
from .forms import MemberForm, OfflineMemberForm, StatusForm
# Create your views here.


def index(request, id=None):
    """Index view. Renders the main page. It'll given Member form, status form and status of the
    Member. The Member form is used to set the Member/tester name.
    """
    context = {
        'status_form': StatusForm()
    }
    if request.session.get('member'):
        try:
            member = Member.objects.get(
                name=request.session['member'])
            context['member_form'] = MemberForm(instance=member)
            status = Status.objects.filter(
                member=member, created_date__date=timezone.now().date())
            context['status'] = status
        except Member.DoesNotExist:
            context['member_form'] = MemberForm()
        if id is not None:
            try:
                status = Status.objects.get(pk=id)
                edit_status_form = StatusForm(instance=status)
                context['status_form'] = edit_status_form
                context['is_status_being_edited'] = True
                context['edit_status_id'] = status.id
            except Status.DoesNotExist:
                messages.warning(
                    request, 'Status with id {0} does not exist'.format(id))
    else:
        context['member_form'] = MemberForm()
        messages.info(request, 'Please set member details first.')
    context['offline_members'] = OfflineMember.objects.filter(
        created_date__date=timezone.now().date())
    context['offline_members_form'] = OfflineMemberForm()
    return render(request, 'status/index.html', context)


@require_POST
def set_member(request):
    """
    view to set Member name in database and in session. If duplicate is found then view will update the member and redirect.
    """
    member_form = MemberForm(request.POST)
    if member_form.is_valid():
        new_member = member_form.save()
        messages.success(
            request, '{0} set successfully.'.format(new_member.name))
        request.session['member'] = new_member.name
    else:
        if member_form.has_error('name', 'unique'):
            try:
                member = Member.objects.get(name=request.POST['name'])
                member_form = MemberForm(request.POST, instance=member)
                if member_form.is_valid():
                    member = member_form.save()
                    messages.success(
                        request, '{0} set successfully.'.format(member.name))
                    request.session['member'] = member.name
                else:
                    messages.error(request, 'Error updating member')
                    messages.error(request, member_form.errors)
            except Member.DoesNotExist:
                messages.error(
                    request, 'Something went wrong. Duplicate name exists but unable to fetch it. Contact developer.')
        else:
            messages.error(request, member_form.errors)

    return redirect('index')


@require_POST
def add_status(request):
    """
    Add Status
    """
    if request.session.get('member'):
        status_form = StatusForm(request.POST)
        if status_form.is_valid():
            status = status_form.save(commit=False)
            status.member = get_object_or_404(
                Member, name=request.session.get('member'))
            status.save()
            messages.success(request, 'Status added successfully.')
        else:
            messages.error(request, status_form.errors)
    else:
        messages.error(request, 'Please set member details first.')
    return redirect('index')


@require_POST
def edit_status(request, id):
    """
    Edit a status based on Id
    """
    try:
        status = Status.objects.get(pk=id)
        edit_status_form = StatusForm(request.POST, instance=status)
        if edit_status_form.is_valid():
            edit_status_form.save()
            messages.success(request, 'Status updated successfully.')
        else:
            messages.error(request, edit_status_form.errors)
    except Status.DoesNotExist:
        messages.warning(
            request, 'Status with id {0} does not exist'.format(id))
    return redirect('index')


@require_GET
def delete_status(request, id):
    """
    Delete a status based on the given id
    """
    try:
        status = Status.objects.get(pk=id)
        status.delete()
        messages.success(
            request, 'Status with id {0} deleted successfully.'.format(id))
    except Status.DoesNotExist:
        messages.warning(
            request, "Status with id {0} doesn't exist".format(id))
    return redirect('index')


@require_GET
def delete_member(request, id):
    """
    Delete member based on Id
    """
    try:
        member = Member.objects.get(pk=id)
        member.delete()
        request.session.flush()
        messages.success(
            request, 'Member with name {0} deleted successfully. Now please try to set member details again.'.format(member.name))
    except Member.DoesNotExist:
        messages.warning(
            request, "Member with id {0} doesn't exist".format(id))
    return redirect('index')


def create_email_within_browser(request):
    """
    Create email with all the members status for present day and display it in browser
    """
    try:
        status = Status.objects.filter(
            created_date__date=timezone.now().date()).order_by('member__name')
        offline_members = OfflineMember.objects.filter(
            created_date__date=timezone.now().date())
        if not status.exists() and not offline_members.exists():
            messages.warning(request, 'No status set for today')
            return redirect('index')
        context = {
            'status': status,
            'offline_members': offline_members
        }
    except Status.DoesNotExist:
        messages.warning(request, 'No status found for today')
        return redirect('index')
    return render(request, 'status/email.html', context)


def create_email_in_outlook(request):
    """
    Generate email and open it in outlook. Beware this view must be called on host machine only.
    """
    status = Status.objects.filter(
        created_date__date=timezone.now().date()).order_by('member__name')
    offline_members = OfflineMember.objects.filter(
        created_date__date=timezone.now().date())
    context = {
        'status': status,
        'offline_members': offline_members
    }
    email_body = loader.render_to_string('status/email.html', context, request)
    with open('mailing_list.yml') as f:
        try:
            mailing_list = load(f, FullLoader)
            print(';'.join(mailing_list['to']))
            pythoncom.CoInitialize()
            outlook = win32.Dispatch('outlook.application')
            mail = outlook.CreateItem(0)
            mail.To = ';'.join(mailing_list['to'])
            mail.CC = ';'.join(mailing_list['cc'])
            mail.Subject = 'Offshore status {0}'.format(
                timezone.localdate().strftime('%m/%d/%Y'))
            mail.HtmlBody = email_body
            mail.Display(False)
        except NameError as error:
            messages.error(request, error)
    return redirect('index')


@require_POST
def add_offline_member(request):
    """
    Add members who are offline for the day
    """
    offline_member_form = OfflineMemberForm(request.POST)
    if offline_member_form.is_valid():
        offline_member = offline_member_form.save()
        messages.success(
            request, '{0} added to offline members for today.'.format(offline_member.name))
    else:
        messages.error(request, offline_member_form.errors)
    return redirect('index')


@require_GET
def delete_offline_member(request, id):
    """
    Delete offline members based on id
    """
    offline_member = get_object_or_404(
        OfflineMember, pk=id, created_date__date=timezone.now().date())
    offline_member.delete()
    messages.success(
        request, 'Member with name {0} deleted successfully from offline members list.'.format(offline_member.name))
    return redirect('index')

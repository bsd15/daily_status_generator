from django.contrib import admin
from .models import Member, OfflineMember, Status
# Register your models here.


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'role')


@admin.register(OfflineMember)
class OfflineMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'role', 'created_date')
    date_hierarchy = 'created_date'
    readonly_fields = ('created_date',)

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ('incident', 'severity', 'member')
    readonly_fields = ('created_date',)
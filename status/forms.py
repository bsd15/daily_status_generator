from django.forms import ModelForm, ChoiceField
from .models import Member, OfflineMember, Status


class MemberForm(ModelForm):
    # role = ChoiceField(choices=Role.choices, required=True)
    class Meta:
        model = Member
        fields = ("name", "role")


class OfflineMemberForm(ModelForm):

    class Meta:
        model = OfflineMember
        fields = ("name", "role")


class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ("incident", "description", "severity", "progress", "comment")
